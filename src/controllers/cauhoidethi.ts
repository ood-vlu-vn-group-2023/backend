import { Request, Response } from "express";
import HttpStatus from "http-status-codes";

import ServiceCauHoiDeThi from "../services/cauhoidethi";
import { ArrayCauHoiDeThi } from "../models/cauhoidethi";
import { LastOrCount } from "../models/alltypes";

async function updateCautraloi(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id;
    const cautraloi_id: number = req.body.cautraloi_id;

    if (isNaN(id) || isNaN(cautraloi_id) || id < 1 || cautraloi_id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Id and cautraloi_id are required." });
      return;
    }

    const service: ServiceCauHoiDeThi = new ServiceCauHoiDeThi();
    service.cauhoidethi_id = id;
    service.cautraloi_id = cautraloi_id;
    const result: boolean = await service.updateCautraloi();

    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Update CauHoiDeThi successfully!" });
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." });
    }
  } catch (error) {
    console.error("Error in ControllerCauHoiDeThi:", error);
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" });
  }
}

async function getAll(req: Request, res: Response): Promise<void> {
  try {
    const limit: number = parseInt(req.query.limit as string, 10);
    const offset: number = parseInt(req.query.offset as string, 10);

    // Kiểm tra dữ liệu đầu vào
    if (isNaN(limit) || isNaN(offset)) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Both limit and offset are required." });
      return;
    }

    const service: ServiceCauHoiDeThi = new ServiceCauHoiDeThi();
    const result: ArrayCauHoiDeThi = await service.getAll(limit, offset);

    if (result) {
      res.status(HttpStatus.OK).json(result);
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." });
    }
  } catch (error) {
    console.error("Error in ControllerCauHoiDeThi:", error);
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" });
  }
}

async function getAllOnDethi(req: Request, res: Response): Promise<void> {
  try {
    const dethi_id: number = parseInt(req.params.dethi_id, 10);

    if (isNaN(dethi_id) || dethi_id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: dethi_id is required." });
      return;
    }

    const service: ServiceCauHoiDeThi = new ServiceCauHoiDeThi();
    service.dethi_id = dethi_id;
    const result: ArrayCauHoiDeThi = await service.getAllOnDethi();

    if (result) {
      res.status(HttpStatus.OK).json(result);
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." });
    }
  } catch (error) {
    console.error("Error in ControllerCauHoiDeThi:", error);
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" });
  }
}

async function getCountOnDethi(req: Request, res: Response): Promise<void> {
  try {
    const dethi_id: number = parseInt(req.params.dethi_id, 10);

    const service: ServiceCauHoiDeThi = new ServiceCauHoiDeThi();
    service.dethi_id = dethi_id;
    const result: LastOrCount = await service.getCountOnDethi();

    if (result) {
      res.status(HttpStatus.OK).json(result);
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." });
    }
  } catch (error) {
    console.error("Error in ControllerCauHoiDeThi:", error);
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" });
  }
}

async function getCountTrue(req: Request, res: Response): Promise<void> {
  try {
    const dethi_id: number = parseInt(req.params.dethi_id, 10);

    const service: ServiceCauHoiDeThi = new ServiceCauHoiDeThi();
    service.dethi_id = dethi_id;
    const result: LastOrCount = await service.getCountTrue();

    if (result) {
      res.status(HttpStatus.OK).json(result);
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." });
    }
  } catch (error) {
    console.error("Error in ControllerCauHoiDeThi:", error);
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" });
  }
}

export default {
  updateCautraloi,
  getAll,
  getAllOnDethi,
  getCountOnDethi,
  getCountTrue,
};
