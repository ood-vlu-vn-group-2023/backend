export type CauTraLoi = {
  cautraloi_id: number
  cauhoi_id: number
  cautraloi_name: string
}

export type OnlyCauTraLoi = CauTraLoi | undefined

export type ArrayCauTraLoi = CauTraLoi[] | undefined