import ServiceCauHoiDeThi from "../../services/cauhoidethi"

test("Trả về tổng giá trị của bảng CauHoiDeThi", async () => {
  let sv: ServiceCauHoiDeThi = new ServiceCauHoiDeThi()
  sv.dethi_id = 1
  const result = await sv.getCount()
  expect(result).not.toBe(undefined)
})

test("Trả về tổng giá trị đúng của bảng CauHoiDeThi", async () => {
  let sv: ServiceCauHoiDeThi = new ServiceCauHoiDeThi()
  sv.dethi_id = 1
  const result = await sv.getCountTrue()
  expect(result).not.toBe(undefined)
})