import express from "express"
import Controller from "../controllers/auth"

const router = express.Router()

router.post("/login", Controller.login)
router.delete("/logout", Controller.logout)
router.get("/checkadmin", Controller.checkadmin)
router.get("/checklogin", Controller.checklogin)

export default router