export type MonHoc = {
  monhoc_id: number
  lophoc_id: number
  monhoc_name: string
}

export type OnlyMonHoc = MonHoc | undefined

export type ArrayMonHoc = MonHoc[] | undefined