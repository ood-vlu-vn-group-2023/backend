export type DangKyLopHoc = {
  dangkylophoc_id: number
  sinhvien_id: number
  lophoc_id: number
}

export type ArrayDangKyLopHoc = DangKyLopHoc[] | undefined