export type CauHoi = {
  cauhoi_id: number
  chuong_id: number
  cautraloi_id: number
  cauhoi_name: string
}
export type OnlyCauHoi = CauHoi | undefined

export type ArrayCauHoi = CauHoi[] | undefined