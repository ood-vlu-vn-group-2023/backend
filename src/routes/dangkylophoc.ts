import express from "express"
import Controller from "../controllers/dangkylophoc"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/all", Controller.getAll)

export default router