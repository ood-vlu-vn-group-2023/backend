import { QueryResult } from "pg"
import { ArraySinhVien, OnlySinhVien, SinhVien } from "../models/sinhvien"
import pool from "../config/database"

class ServiceSinhVien {
  sinhvien_id!: number
  nguoidung_account!: string
  sinhvien_name!: string
  sinhvien_gender!: string
  sinhvien_birthday!: string

  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_sinhvien($1, $2, $3, $4);",
        values: [
          this.nguoidung_account,
          this.sinhvien_name,
          this.sinhvien_gender,
          this.sinhvien_birthday
        ]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      if (this.sinhvien_id !== undefined)
        await pool.query({
          text: "CALL update_sinhvien($1, $2, $3, $4, $5);",
          values: [
            this.sinhvien_id,
            this.nguoidung_account,
            this.sinhvien_name,
            this.sinhvien_gender,
            this.sinhvien_birthday
          ]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_sinhvien($1);",
        values: [this.sinhvien_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async getOnAccount(username: string): Promise<OnlySinhVien> {
    let value: OnlySinhVien

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM get_sinhvien_withaccount($1);",
        values: [username],
      })

      // Convert result row to CauHoi and add to allValues
      value = result.rows[0]
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return value
  }

  async getAll(limit: number, offset: number): Promise<ArraySinhVien> {
    let allValues: ArraySinhVien = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_sinhvien_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: SinhVien) => ({
        sinhvien_id: row.sinhvien_id,
        nguoidung_account: row.nguoidung_account,
        sinhvien_name: row.sinhvien_name,
        sinhvien_gender: row.sinhvien_gender,
        sinhvien_birthday: row.sinhvien_birthday
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceSinhVien