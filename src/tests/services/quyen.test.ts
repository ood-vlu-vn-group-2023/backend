import { OnlyQuyen } from "../../models/quyen"
import ServiceQuyen from "../../services/quyen"

test("Lấy dữ liệu nào chứa quyền admin", async () => {
  let sv: ServiceQuyen = new ServiceQuyen()
  const result: OnlyQuyen = await sv.getAdmin()
  expect(result).not.toBe(undefined)
})