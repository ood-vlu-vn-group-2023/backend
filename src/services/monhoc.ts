import { QueryResult } from "pg"

import pool from "../config/database"
import { ArrayMonHoc, MonHoc, OnlyMonHoc } from "../models/monhoc"

class ServiceMonhoc {
  monhoc_id!: number
  lophoc_id!: number
  monhoc_name!: string

  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_monhoc($1, $2);",
        values: [this.lophoc_id, this.monhoc_name],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }
  async update(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL update_monhoc($1, $2, $3);",
        values: [this.monhoc_id, this.lophoc_id, this.monhoc_name],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_monhoc($1);",
        values: [this.monhoc_id],
      })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async getAll(limit: number, offset: number): Promise<ArrayMonHoc> {
    let allValues: ArrayMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_monhoc_withlimitoffset($1, $2)",
        values: [limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: MonHoc) => ({
        monhoc_id: row.monhoc_id,
        lophoc_id: row.lophoc_id,
        monhoc_name: row.monhoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnSinhvien(sinhvien_id: number): Promise<ArrayMonHoc> {
    let allValues: ArrayMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_monhoc_on_sinhvien($1);",
        values: [sinhvien_id]
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: MonHoc) => ({
        monhoc_id: row.monhoc_id,
        lophoc_id: row.lophoc_id,
        monhoc_name: row.monhoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async searchAll(keyword: string, limit: number, offset: number): Promise<ArrayMonHoc> {
    let allValues: ArrayMonHoc = []

    try {
      // Execute SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM searchall_monhoc($1, $2, $3)",
        values: [keyword, limit, offset],
      })

      // Convert result row to CauHoi and add to allValues
      allValues = result.rows.map((row: MonHoc) => ({
        monhoc_id: row.monhoc_id,
        lophoc_id: row.lophoc_id,
        monhoc_name: row.monhoc_name,
      }))
    } catch (error) {
      // Return undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceMonhoc