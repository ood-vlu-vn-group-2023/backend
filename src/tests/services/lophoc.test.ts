import ServiceLopHoc from "../../services/lophoc"

test("Trả về hết bảng LopHoc dựa trên limit và page", async () => {
  let sv: ServiceLopHoc = new ServiceLopHoc()
  const result = await sv.getAll(10, 0)
  expect(result).not.toBe(undefined)
})

test("Trả về bảng LopHoc dựa trên sinhvien_id", async () => {
  let sv: ServiceLopHoc = new ServiceLopHoc()
  const result = await sv.getOnSinhvien(1)
  expect(result).not.toBe(undefined)
})