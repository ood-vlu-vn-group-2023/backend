import ServiceMonhoc from "../../services/monhoc"

test("Trả về hết bảng MonHoc dựa trên sinhvien_id", async () => {
  let sv: ServiceMonhoc = new ServiceMonhoc()
  const result = await sv.getAllOnSinhvien(2)
  expect(result).not.toBe(undefined)
})