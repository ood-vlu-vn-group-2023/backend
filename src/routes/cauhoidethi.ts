import express from "express"
import Controller from "../controllers/cauhoidethi"

const router = express.Router()

router.put("/update/cautraloi", Controller.updateCautraloi)
router.get("/all", Controller.getAll)
router.get("/dethi/:dethi_id", Controller.getAllOnDethi)
router.get("/count-dethi/:dethi_id", Controller.getCountOnDethi)
router.get("/count-true/:dethi_id", Controller.getCountTrue)

export default router