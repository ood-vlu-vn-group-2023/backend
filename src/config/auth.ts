const JWT_SECRET_ADMIN: string = process.env.JWT_SECRET_ADMIN || "AdminTrungTamLuyenThi"
const JWT_SECRET_USER: string = process.env.JWT_SECRET_USER || "UserTrungTamLuyenThi"

export { JWT_SECRET_ADMIN, JWT_SECRET_USER }