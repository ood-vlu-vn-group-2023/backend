import { QueryResult } from "pg"
import pool from "../config/database"
import { ArrayThoiGianDeThiChinh, ThoiGianDeThiChinh } from "../models/thoigiandethichinh"

class ServiceThoiGianDeThiChinh {
  thoigiandethichinh_id!: number
  dethimonhoc_id!: number
  thoigiandethichinh_time!: string

  /* Các method cập nhập dữ liệu */
  async insert(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL insert_thoigiandethichinh($1, $2);",
        values: [this.dethimonhoc_id, this.thoigiandethichinh_time]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async update(): Promise<boolean> {
    try {
      if (this.thoigiandethichinh_id !== undefined)
        await pool.query({
          text: "CALL update_thoigiandethichinh($1, $2, $3);",
          values: [
            this.thoigiandethichinh_id,
            this.dethimonhoc_id,
            this.thoigiandethichinh_time
          ]
        })
    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  async remove(): Promise<boolean> {
    try {
      await pool.query({
        text: "CALL delete_thoigiandethichinh($1);",
        values: [this.thoigiandethichinh_id]
      })

    } catch (error) {
      console.log(error)
      return false
    }

    return true
  }

  /* Các method lấy dữ liệu */
  async getAll(limit: number, offset: number): Promise<ArrayThoiGianDeThiChinh>
  {
    let allValues: ArrayThoiGianDeThiChinh = []

    try {
      // Chạy lệnh SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_thoigiandethichinh_withlimitoffset($1, $2);",
        values: [limit, offset],
      })

      // chuyển đổi result rows sang ThoiGianDeThiChinh và thêm vô allValues
      allValues = result.rows.map((row: ThoiGianDeThiChinh) => ({
        thoigiandethichinh_id: row.thoigiandethichinh_id,
        dethimonhoc_id: row.dethimonhoc_id,
        thoigiandethichinh_time: row.thoigiandethichinh_time
      }))
    } catch (error) {
      // Trả về undefined
      console.log(error)
      return undefined
    }

    return allValues
  }

  async getAllOnSinhvien(sinhvien_id: number): Promise<ArrayThoiGianDeThiChinh>
  {
    let allValues: ArrayThoiGianDeThiChinh = []

    try {
      // Chạy lệnh SQL
      const result: QueryResult = await pool.query({
        text: "SELECT * FROM getall_thoigiandethichinh_on_sinhvien($1);",
        values: [sinhvien_id],
      })

      // chuyển đổi result rows sang ThoiGianDeThiChinh và thêm vô allValues
      allValues = result.rows.map((row: ThoiGianDeThiChinh) => ({
        thoigiandethichinh_id: row.thoigiandethichinh_id,
        dethimonhoc_id: row.dethimonhoc_id,
        thoigiandethichinh_time: row.thoigiandethichinh_time
      }))
    } catch (error) {
      // Trả về undefined
      console.log(error)
      return undefined
    }

    return allValues
  }
}

export default ServiceThoiGianDeThiChinh