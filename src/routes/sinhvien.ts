import express from "express"
import Controller from "../controllers/sinhvien"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/username/:username", Controller.getOnAccount)
router.get("/all", Controller.getAll)

export default router