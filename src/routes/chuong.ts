import express from "express"
import Controller from "../controllers/chuong"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/all", Controller.getAll)
router.get("/monhoc/:monhoc_id", Controller.getAllChuongOnMonHoc)
router.get("/count", Controller.getCount)
router.get("/count-monhoc/:monhoc_id", Controller.getCountOnMonhoc)

export default router