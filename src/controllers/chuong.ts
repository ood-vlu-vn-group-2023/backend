import { Request, Response } from "express"
import ServiceChuong from "../services/chuong"
import HttpStatus from "http-status-codes"
import { ArrayChuong, OnlyChuong } from "../models/chuong"
import { LastOrCount } from "../models/alltypes"

async function insert(req: Request, res: Response) {
  try {
    const monhoc_id: number = req.body.monhoc_id
    const number: number = req.body.chuong_number
    const name: string = req.body.name
    if (
      isNaN(monhoc_id) || monhoc_id < 1 ||
      isNaN(number) || number < 1 ||
      !name) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: monhoc_id, chuong_number and name are required." })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    service.chuong_name = name
    service.monhoc_id = monhoc_id
    service.chuong_number = number
    const result: boolean = await service.insert()
    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Insert Chuong successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function update(req: Request, res: Response): Promise<void> {
  try {
    const id: number = req.body.id
    const monhoc_id: number = req.body.monhoc_id
    const number: number = req.body.number
    const name: string = req.body.name

    if (
      isNaN(monhoc_id) || monhoc_id < 1 ||
      isNaN(id) || id < 1 ||
      isNaN(number) || number < 1
    ) {
      res.status(HttpStatus.BAD_REQUEST).json({
        message:
          "Bad Request: Id, monhoc_id, number are required and name is optional.",
      })
      return
    }
    const service: ServiceChuong = new ServiceChuong()
    service.chuong_id = id
    service.monhoc_id = monhoc_id
    service.chuong_number = number
    service.chuong_name = name
    const result: boolean = await service.update()
    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Update Chuong successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function remove(req: Request, res: Response): Promise<void> {
  try {
    const id: number = parseInt(req.params.id as string, 10)

    if (isNaN(id) || id < 1) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: id is required." })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    service.chuong_id = id
    const result: boolean = await service.remove()

    if (result) {
      res
        .status(HttpStatus.OK)
        .json({ message: "Remove Chuong successfully!" })
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerLopHoc:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function getAll(req: Request, res: Response) {
  try {
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)
    if (isNaN(limit) || isNaN(offset)) {
      res
        .status(HttpStatus.BAD_REQUEST)
        .json({ message: "Bad Request: Both limit and offset are required." })
      return
    }
    const service = new ServiceChuong()
    const result = await service.getAll(limit, offset)
    if (result) {
      res.json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}

async function getAllChuongOnMonHoc(req: Request, res: Response): Promise<void> {
  try {
    const monhoc_id: number = parseInt(req.params.monhoc_id, 10)
    const limit: number = parseInt(req.query.limit as string, 10)
    const offset: number = parseInt(req.query.offset as string, 10)

    if (
      isNaN(monhoc_id) ||monhoc_id < 1 ||
      isNaN(limit) || isNaN(offset)
    ) {
      res.status(HttpStatus.BAD_REQUEST).json({
        message:
          "Bad Request: Both monhoc_id param, limit and offset query are required.",
      })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    service.monhoc_id = monhoc_id
    const result: ArrayChuong = await service.getAllChuongOnMonHoc()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function getCount(req: Request, res: Response): Promise<void> {
  try {
    const service: ServiceChuong = new ServiceChuong()
    const result: LastOrCount = await service.getCount()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function getCountOnMonhoc(req: Request, res: Response): Promise<void> {
  try {
    const monhoc_id: number = parseInt(req.params.monhoc_id as string, 10)

    const service: ServiceChuong = new ServiceChuong()
    service.monhoc_id = monhoc_id
    const result: LastOrCount = await service.getCountOnMonhoc()

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res
      .status(HttpStatus.INTERNAL_SERVER_ERROR)
      .json({ message: "Internal Server Error" })
  }
}

async function searchAll(req: Request, res: Response): Promise<void> {
  try {
    const keyword: string = req.params.keyword
    const limit: number = parseInt(req.query.limit as string)
    const offset: number = parseInt(req.query.offset as string)

    if (!keyword || isNaN(limit) || isNaN(offset)) {
      res.status(HttpStatus.BAD_REQUEST).json({ message: "Bad Request: Both keyword, keyword, limit and offset are required." })
      return
    }

    const service: ServiceChuong = new ServiceChuong()
    const result: ArrayChuong = await service.searchAll(keyword, limit, offset)

    if (result) {
      res.status(HttpStatus.OK).json(result)
    } else {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error: Unable to fetch data." })
    }
  } catch (error) {
    console.error("Error in ControllerChuong:", error)
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: "Internal Server Error" })
  }
}
export default {
  insert, remove, update,
  getAll, getAllChuongOnMonHoc, getCount, getCountOnMonhoc,
  searchAll
}