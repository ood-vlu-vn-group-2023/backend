import express from "express"
import Controller from "../controllers/lophoc"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/id/:id", Controller.get)
router.get("/all", Controller.getAll)
router.get("/countmonhoc/:id", Controller.getCountMonhoc)
router.get("/countsinhvien/:id", Controller.getCountSinhvien)
router.get("/sinhvien/:sinhvien_id", Controller.getOnSinhvien)
router.get("/search/:keyword", Controller.searchAll)

export default router