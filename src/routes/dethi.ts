import express from "express";
import Controller from "../controllers/dethi";

const router = express.Router();

router.post("/dethithu", Controller.insertThu);
router.post("/dethichinh", Controller.insertChinh);
router.get("/all", Controller.getAll);
router.get("/sinhvien/:sinhvien_id", Controller.getAllOnSinhVien);
router.get("/count", Controller.getCount);
router.get("/id/:id", Controller.get);

export default router;
