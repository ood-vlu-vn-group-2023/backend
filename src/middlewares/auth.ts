import jwt from "jsonwebtoken"
import { Request, Response, NextFunction } from "express"
import { JWT_SECRET_ADMIN, JWT_SECRET_USER } from "../config/auth"

// Cấu trúc dữ liệu lưu trữ quyền truy cập của từng vai trò
const accessRoles = {
  user: {
    allowedPaths: [
      "/api/auth",
      "/api/cauhoidethi/update/cautraloi",
      "/api/dethi/:dethi_id",
      "/api/lophoc/sinhvien/:sinhvien_id",
      "/api/monhoc/sinhvien/:sinhvien_id",
      "/api/nguoidung/update/info",
      "/api/nguoidung/username/:username",
      "/api/sinhvien/username/:username",
      "/api/thoigiandethichinh/sinhvien/:sinhvien_id"
    ],
    restrictedPaths: [
      "/api/cauhoi",
      "/api/cautraloi",
      "/api/chuong",
      "/api/lophoc/insert",
      "/api/lophoc/update",
      "/api/lophoc/remove",
      "/api/monhoc/insert",
      "/api/monhoc/update",
      "/api/monhoc/remove",
      "/api/nguoidung/insert",
      "/api/nguoidung/update",
      "/api/nguoidung/remove",
      "/api/nguoidung/all",
      "/api/quyen",
      "/api/sinhvien/insert",
      "/api/sinhvien/update",
      "/api/sinhvien/remove",
      "/api/sinhvien/all",
      "/api/thoigiandethichinh/all",
      "/api/thoigiandethichinh/insert",
      "/api/thoigiandethichinh/update",
      "/api/thoigiandethichinh/remove"
    ]
  },
  public: {
    allowedPaths: ["/api/auth/"] // Thêm đường dẫn công cộng vào đây
  }
}

function authenticateWithCookie(req: Request, res: Response, next: NextFunction): void {
  try {
    // Lấy token từ cookie
    const token: string = req.cookies["login-token"]

    // Kiểm tra trạng thái đăng nhập
    if (!token) {
      const role = "public"
      // Kiểm tra xem đường dẫn hiện tại có nằm trong danh sách đường dẫn công cộng hay không
      const allowedPaths = accessRoles[role].allowedPaths

      if (allowedPaths.some((allowedPath: string) => req.path.startsWith(allowedPath))) {
        // Nếu đường dẫn thuộc danh sách đường dẫn công cộng, cho phép tiếp tục xử lý
        next()
      } else {
        // Nếu không phải đường dẫn công cộng, trả về lỗi 401
        res.status(401).json({ message: "Unauthorized: No token provided" })
      }
      return
    }

    // Xác thực token sử dụng JWT_SECRET_ADMIN
    jwt.verify(token, JWT_SECRET_ADMIN, (adminErr: jwt.VerifyErrors | null) => {
      if (!adminErr) {
        // Nếu không có lỗi với JWT_SECRET_ADMIN, tiếp tục xử lý
        (req as any).admin = true
        next()
        return
      }

      // Nếu có lỗi với JWT_SECRET_ADMIN, thử xác thực với JWT_SECRET_USER
      jwt.verify(token, JWT_SECRET_USER, (userErr: jwt.VerifyErrors | null, userDecoded: any) => {
        if (userErr) {
          // Nếu cả hai đều lỗi, trả về lỗi 401 (Unauthorized)
          res.status(401).json({ message: "Unauthorized: Invalid token" })
          return
        }

        // Lưu thông tin người dùng từ token vào req để sử dụng trong các xử lý tiếp theo
        (req as any).user = userDecoded

        // Kiểm tra đường dẫn của request để xác định quyền truy cập cho người dùng
        const allowedPaths = accessRoles.user.allowedPaths
        const restrictedPaths = accessRoles.user.restrictedPaths

        if (allowedPaths.some((allowedPath: string) => req.path.startsWith(allowedPath))) {
          // Người dùng được truy cập vào đường dẫn được phép, tiếp tục xử lý
          next()
        } else if (restrictedPaths.some((restrictedPath: string) => req.path.startsWith(restrictedPath))) {
          // Nếu đường dẫn bắt đầu bằng một đường dẫn bị hạn chế, trả về lỗi 401
          res.status(401).json({ message: "Unauthorized: Insufficient permissions" })
        } else {
          // Nếu không phải là đường dẫn phù hợp, tiếp tục xử lý
          next()
        }
      })
    })
  } catch (error) {
    console.error("Error in authentication middleware:", error)
    res.status(500).json({ message: "Internal Server Error" })
  }
}

export default authenticateWithCookie
