import express from "express"
import Controller from "../controllers/cautraloi"

const router = express.Router()

router.post("/insert", Controller.insert)
router.put("/update", Controller.update)
router.delete("/remove/:id", Controller.remove)
router.get("/id/:id", Controller.get)
router.get("/all", Controller.getAll)
router.get("/cauhoi/:cauhoi_id", Controller.getAllOnCauHoi)
router.get("/count", Controller.getCount)
router.get("/search/:keyword", Controller.searchAll)

export default router