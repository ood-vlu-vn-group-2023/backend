// Lấy thư viện hệ thống
import dotenv from "dotenv"
import cors from "cors"
import express from "express"
import cookieParser from "cookie-parser"
import morgan from "morgan"

// Lấy thư viện tự tạo
import authenticateWithCookie from "./middlewares/auth"

import RouterAuth from "./routes/auth"
import RouterCauHoi from "./routes/cauhoi"
import RouterCauHoiDeThi from "./routes/cauhoidethi"
import RouterCauTraLoi from "./routes/cautraloi"
import RouterChuong from "./routes/chuong"
import RouterDangKyLopHoc from "./routes/dangkylophoc"
import RouterDeThi from "./routes/dethi"
import RouterDeThiMonHoc from "./routes/dethimonhoc"
import RouterLopHoc from "./routes/lophoc"
import RouterMonhoc from "./routes/monhoc"
import RouterNguoiDung from "./routes/nguoidung"
import RouterQuyen from "./routes/quyen"
import RouterSinhVien from "./routes/sinhvien"
import RouterThoiGianDeThiChinh from "./routes/thoigiandethichinh"

// Setup các biến
dotenv.config()
const ADDRESS: string = process.env.API_ADDRESS || "localhost"
const PORT: number = parseInt(process.env.API_PORT as string, 10) || 3000
const app = express()

// Sử dụng các thư viện
app.use(express.json())
app.use(cors({
  credentials: true,
  origin: 'http://localhost:5173', // địa chỉ của frontend
}))
app.use(morgan("tiny"))
app.use(cookieParser())
app.use(authenticateWithCookie)

// Gom các routes
app.use("/api/auth", RouterAuth)
app.use("/api/cauhoi", RouterCauHoi)
app.use("/api/cautraloi", RouterCauTraLoi)
app.use("/api/chuong", RouterChuong)
app.use("/api/dangkylophoc", RouterDangKyLopHoc)
app.use("/api/dethi", RouterDeThi)
app.use("/api/dethimonhoc", RouterDeThiMonHoc)
app.use("/api/cauhoidethi", RouterCauHoiDeThi)
app.use("/api/lophoc", RouterLopHoc)
app.use("/api/monhoc", RouterMonhoc)
app.use("/api/nguoidung", RouterNguoiDung)
app.use("/api/quyen", RouterQuyen)
app.use("/api/sinhvien", RouterSinhVien)
app.use("/api/thoigiandethichinh", RouterThoiGianDeThiChinh)

// Khởi động server
app.listen(PORT, () => {
  console.log(`Server is running at http://${ADDRESS}:${PORT}`)
})