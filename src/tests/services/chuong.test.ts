import ServiceChuong from "../../services/chuong"
test("Trả về hết bảng Chuong dựa trên limit và offset", async () => {
  let sv: ServiceChuong = new ServiceChuong()
  const result = await sv.getAll(10, 0)
  expect(result).not.toBe(undefined)
})

test("Nhập dữ liệu vào bảng CauHoi", async () => {
  let sv: ServiceChuong = new ServiceChuong()
  sv.chuong_id = 1
  sv.chuong_name = "Khoa học dữ liệu"
  sv.chuong_number = 2
  const result = await sv.insert()
  expect(result).not.toBe(false)
})

test("Cập nhập thông tin của bảng Chuong", async () => {
  let sv: ServiceChuong = new ServiceChuong()
  sv.chuong_id = 76
  sv.monhoc_id = 2
  sv.chuong_name = "Khang đẹp trai năm nào?"
  const result = await sv.update()
  expect(result).not.toBe(false)
})
